# 🔌⚙️ DevToolsMod
This [Raft](https://raft-game.com/) mod adds *hotloading mods* to the game which allows you to be more flexible with your development environment. This might for example be useful if you are using an IDE that requires configuration files etc, which are not allowed in the mods folder.

## Hotloading
* To hotload a mod open the console and type in `hotload` <`file`> (replacing file with the full file path to the .cs file you want to load) and hit enter. 
* The mod should now be loaded and will be re-loaded once you change the file and save it.
* To unload the hotloaded mod, open the console and type `hotload unload`.
* Use the `hotload` command at any time to get the status of the hotload feature.

## Console mod management
The `mods` command allows you to manage mods loaded via the RaftModLoader.
* `mods` prints a list of all mods loaded by RML to the console.
* `mods help` prints a help message for the `mods` command directly to the console.
* `mods refresh` refreshes the RML mod list. This is equivalent to pressing the refresh button in the mods menu.
* `mods reload` <`modname`>

## Source code and issues
If you have any ideas or issues with this mod, please contact me on Discord (`traxam#7012`) or open an issue in the [git repository](https://gitlab.com/traxam/raft-devtoolsmod).
* [git repository on GitLab](https://gitlab.com/traxam/raft-devtoolsmod)
* Icon by [Freepik](https://www.freepik.com/) from [Flaticon](https://www.flaticon.com), licensed CC 3.0 BY
* [Raft-mods entry](https://raft-mods.trax.am/mods/devtools)